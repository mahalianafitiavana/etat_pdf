TODO - 01 Juin 2021

- objectifs : 
	- créer un fichier PDF de l'image "modele-note"

- préparation :
	- test de la librairie PDF sous PHP - fpdf17.zip
		- lire fichier tutorial/index.htm et parcourir les différents tutos
	- création structure base de données
	- insertion des données de test

- finalisation :
	- créer une page php qui affiche les notes et un bouton "Exporter PDF" pour exporter en PDF

